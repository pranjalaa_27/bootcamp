#Student Record Questions Approach 1
from itertools import combinations as nCr
import sys
'''
FILE = sys.argv[1]

def student_data(FILE) -> list[list]:
    return [[line[0]] + list(map(int, line[1:].strip('\n').split())) for line in open(FILE)]


def compare_subjects(FILE) -> list[list]:
    return [[f'{x[0]}>{y[0]}' if a > b else f'{x[0]}<{y[0]}' for a,b in zip(x[1:], y[1:])] for x, y in combinations(student_data(FILE), 2)]


def compare_students(FILE):
    return [relation[0] if relation.count(relation[0]) == len(relation) else relation[0][0] + '#' + relation[0][2] for relation in compare_subjects(FILE)]

def student_ranking(FILE):
    return compare_students(FILE)

print(student_ranking(FILE))
'''
#Student Record Questions Approach 2
COMMENT = '#'
LT, GT, NE = '<', '>', '#'

#AS A DICTIONARY
'''
def load_data(filename: str) -> dict[str, list[int]]:
    data = dict()
    with open(filename) as f:
        for line in f:
            if line[0] != COMMENT:
                name, *raw_marks = line.split()
                data[name] = [int(r) for r in raw_marks]
    return data

def relation(a_student: list[int], b_student: list[int]) -> str:
    if all(a < b for (a,b) in zip(a_student, b_student)):
        return LT
    elif all(a > b for (a,b) in zip(a_student,b_student)):
        return GT
    else:
        return NE
    
def all_relations(filename: str) -> list[str]:
    data = load_data(filename)
    pairs = nCr(data.keys(),2)
    relations = set()
    for pair in pairs:
        rel = relation(data[pair[0]], data[pair[1]])
        s_rel = pair[0] + rel + pair[1]
        relations.add(f'{pair[0]}{rel}{pair[1]}')
    return relations
    
#AS A TUPLE - PREFFERED BECAUSE THE DICTIONARY DOESN'T ADD ANY VALUE
def load_data(filename: str) -> list[tuple[str, list[int]]]:
    data = list()
    with open(filename) as f:
        for line in f:
            if line[0] != COMMENT:
                name, *raw_marks = line.split()
                data.append((name, [int(r) for r in raw_marks]))
    return data

def relation(a_student: tuple[str, list[int]], b_student: tuple[str, list[int]]) -> str:
    a_name,a_marks= a_student
    b_name, b_marks = b_student
    if all(a < b for (a,b) in zip(a_marks, b_marks)):
        return a_name + LT + b_name
    elif all(a > b for (a,b) in zip(a_marks,b_marks)):
        return b_name + LT + a_name
    else:
        return a_name + NE + b_name
    
def all_relations(filename: str) -> list[str]:
    data = load_data(filename)
    return {relation(*pair) for pair in nCr(data,2)}

def comparables(filename: str) ->list[str]:
    return {r for r in all_relations(filename) if NE not in r}

def remove_redundancies(relations: list[str]) -> list[str]:
    firsts = {r[0] for r in relations}
    seconds = {r[-1] for r in relations}
    both = firsts and seconds
    only_firsts = firsts - both
    only_seconds = seconds - both
    redundancies = set()
    for f in only_firsts:
        for s in only_seconds:
            for b in both:
                if f + LT + s in relations and\
                      f + LT + b in relations and\
                          b + LT + s in relations:
                    redundancies.add( f + LT + s)
    return redundancies

print(load_data("student_records.txt"))
print(all_relations("student_records.txt"))
print(comparables("student_records.txt"))
print(remove_redundancies("student_records.txt"))
'''
