#QUESTION
'''Input: Scores of students; n students; m subjects
	Each line is m marks -- all are non-zero
The marks are unique across each subject; that is no two students get the same mark in the same subject.

if each mark of a student S is greater than or equal to the corresponding mark of T, then we say S > T
if each mark of a student S is less than the corresponding mark of T, then we say S < T

if some marks of S are greater and some are lesser we say that they are uncomparable, denoted by A # B

Of course if A > B and B > C then A > C

Write a program that reads the input and outputs the smallest number of lines of the form A > B or A # B to represent the final status completely.

That is the program should not output A > C if A > C and B > C are output

sample data
-----------
A 12 14 16
B 5 6  7
C 17 20 23
D 2 40 12
E 3 41 13
F 7 8 9
G 4 5 6
'''
def store_data(n: int, m: int) -> dict:
    d = {}
    for i in range(n):
        d[input()] = tuple([int(input()) for x in range(m)])
    return d

import sys
from itertools import combinations, permutations

FILE = sys.argv[1]

def student_data(FILE) -> dict[tuple]:
    d = {}
    student_marks = [tuple(map(int, line[1:].strip('\n').split())) for line in open(FILE)]
    student_names = [line[0] for line in open(FILE)]
    for i in range(len(student_names)):
        d[student_names[i]] = tuple(student_marks[i])
    return d 

def compare_subjects(FILE) -> list[list]:
    return [[f'{x[0]}>{y[0]}' if a > b else f'{x[0]}<{y[0]}' for a,b in zip(x[1:], y[1:])] for x, y in combinations(student_data(FILE).values(), 2)]

def compare_students(FILE):
    return [relation[0] if relation.count(relation[0]) == len(relation) else relation[0][0] + '#' + relation[0][2] for relation in compare_subjects(FILE)]


print(compare_students(FILE))

