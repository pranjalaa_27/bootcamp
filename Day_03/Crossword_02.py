
import sys
import pprint

LF, BLACK, WHITE = "\n", "b", "w"

gridfile = sys.argv[1]
DEBUG = len(sys.argv) == 3

grid = []
def display_grid(grid: list) -> list :
    for line in open(sys.argv[1]):
        grid.append(line.strip(LF))
    if DEBUG:
        pprint.pprint(grid)

def add_sentinal(grid: list) -> list:
    size = len(grid)
    grid.insert(0,  BLACK * size)
    grid.append(BLACK * size)

grid = [BLACK + line + BLACK for line in grid]
if DEBUG:
    pprint.pprint(grid)

def transpose_grid():
    transpose = []
    for col in range(len(grid)):
        s = ''
        for row in grid:
            s += row[col]
        transpose.append(s)
    if DEBUG:
        pprint.pprint(transpose)  #prints data in a pretty manner 

def combine_into_lines():
    across = ''.join(grid)