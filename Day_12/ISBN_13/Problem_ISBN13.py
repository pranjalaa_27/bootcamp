# Implement the algorithm that computes the check-digit for ISBN 13
def check_isbn(isbn: str) -> int:
    incomplete_isbn = isbn.replace('-', '')
    sum_of_isbn13 = [int(incomplete_isbn[i]) if i % 2 == 0 else (int(incomplete_isbn[i]) * 3) for i in range(len(incomplete_isbn))]
    return (10 - (sum(sum_of_isbn13) % 10))

def complete_isbn(isbn: str) -> str:
    digit = check_isbn(isbn)
    return isbn + str(digit)

print(complete_isbn("978-0-596-52068-"))
print(complete_isbn("978030640615"))
print(complete_isbn("978186197271"))
