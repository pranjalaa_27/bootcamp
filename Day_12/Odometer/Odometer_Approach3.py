# Reducing time complexity 

def get_reading(reading: str) -> list[int]:
    return [int(i) for i in reading][::-1]
    

    