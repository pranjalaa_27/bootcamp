#Another approach for the Odometer Question
def is_ascending(n: int) -> bool:
    if n < 10:
        return True
    elif (n // 10) % 10 >= n % 10:
        return False
    else:
        return is_ascending( n // 10)
    
def limits(n: int) -> tuple[int, int]:
    DIGITS = '12345678'
    size = len(str(n))
    return int(DIGITS[:size]), int(DIGITS[-size:])

def next_reading(reading: int) -> int:
    start, limit = limits(reading)
    if reading == limit:
        return start
    else:
        reading += 1
        while not is_ascending(reading):
            reading += 1
        return reading
    
def forward(reading: int, steps: int) -> int:
    start, limit = limits(reading)
    for _ in range(steps):
        if reading == limit:
            reading = start
    else:
        reading += 1
        while not is_ascending(reading):
            reading += 1
    return reading
    
def reading_difference(reading1: int, reading2: int) -> int:
    count = 0
    while reading2 != reading1:
        reading1 = next_reading(reading1)
        count += 1
    return count

print(reading_difference(234, 567))