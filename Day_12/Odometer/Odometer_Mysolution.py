MAX_VAL = 789
MIN_VAL = 123
def strictly_ascending(num: str) -> bool:
    return all( a < b < c for a, b, c in zip(num, num[1:], num[2:]))

def next_reading(num: str) -> int:
    for reading in range(int(num) + 1, MAX_VAL):
        if strictly_ascending(str(reading)):
            return int(reading)

def previous_reading(num: str) -> int:
    previous_readings = []
    for reading in range(int(num) - 1, MIN_VAL - 1, -1):
        if strictly_ascending(str(reading)):
            return int(reading)
    

def reading_poststep(num: str, step: int) -> int:
    next_readings = []
    for reading in range(int(num) + 1, MAX_VAL):
        if strictly_ascending(str(reading)):
            next_readings.append(int(reading)) 
    return next_readings[step - 1]

def reading_prevstep(num: str, step: int) -> int:
    prev_readings = []
    for reading in range(int(num) - 1, MIN_VAL - 1, -1):
        if strictly_ascending(str(reading)):
            prev_readings.append(int(reading))
     
    return prev_readings[step - 1]

def reading_difference(reading1: str, reading2: str):
    count = 0
    while int(reading2) != int(reading1):
        reading1 = next_reading(reading1)
        count += 1
    return count

    
print(next_reading('456'))
print(reading_difference('123','456'))

print(reading_poststep('234', 4))