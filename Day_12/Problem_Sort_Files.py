BLANK = ''
def sort_files(filenames: list[str]) -> list[str]:
    sorted_files = sorted(filenames, key=lambda rec:clean(rec))
    return sorted_files

def clean(filename: str) -> tuple[str]:
    file_name = BLANK.join([f for f in filename if not f.isdigit()])
    file_num = int(BLANK.join([str(f) for f in filename if f.isdigit()]))
    return [file_name, file_num]


print(sort_files(['File1','Day12','File232','File111','Day12m','Day12b']))