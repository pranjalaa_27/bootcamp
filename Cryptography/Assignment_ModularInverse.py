def inverse_modulo(a: int, b: int) -> int:
    r1 = a; r2 = b
    y1 = 0; y2 = 1
    q = 0
    while r2 > 1:
        q = r1 // r2 
        r1, r2 = r2 , r1 - (q * r2)
        y1, y2 = y2, y1 - (q * y2)
    return y2

print(inverse_modulo(181, 27))
