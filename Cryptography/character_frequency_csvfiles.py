import random
def get_freq(filename) -> list[list]:
    with open(filename, 'r') as freq:
        char_freq = []
        char = []
        for line in freq:
            data = line.strip('\n').strip('%').split(',')
            char.append(data[0])
            char_freq.append(data[1]) 
        return char[1:], char_freq[1:]

def get_string(filename: str, num: int) -> str:
    char = get_freq(filename)
    for i in range(len(char[0])): 
        char[1][i] = round(float(char[1][i])*0.01 , 4)
    string_req =''.join(random.choices(char[0], weights = char[1], k = num)) 
    return string_req


print(get_string('letter_frequencies.csv', 22))