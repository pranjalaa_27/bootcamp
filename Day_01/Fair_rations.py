#1. Problem 1- Fair Rations(Loaves of Bread) Hackerrank
'''
#Note: The same problem can be solved by checking for even integers and modifying the code accordingly
def checkodd(k : int) -> int:
    return k % 2 != 0

def checkloaves(queue: list[int]) -> str:
    if checkodd(sum(queue)):
        return "NO"
    else:
        return str(fairrations(queue))

def fairrations(queue: list[int]) -> int:
    if len(queue) == 1:
        return 0
        
    elif checkodd(queue[0]):
        return 2 + fairrations([[queue[1] + 1] + queue[2 : ]])
    else:
        return fairrations(queue[1:])
print(checkloaves([2,3,4,2]))
'''

