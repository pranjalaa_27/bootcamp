#Problem to write the polynomial in the way written by Mathematicians (reversed)
s = 'x^2+4x-5'
s1= '5+4x-14x^2+21x^3'
CARET = '^'
def get_var(poly: str) -> str:
    for ch in poly:
        if ch.isalpha():
            return ch
        
def terms(poly: str) -> list[str]:
    poly = ''.join([ch for ch in poly if ch.isspace() == False])
    if poly[0].isdigit():
        poly = '+' + poly
    poly = poly.replace('-','+'+'-')
    return poly.split('+')

def reformat_terms( term: str, var: str) -> tuple[int,int]:
    if var not in term:
        return 0, int(term)
    else:
        coefficient , rest = term.split(var)
        if CARET not in rest:
            return 1, int(coefficient)
        else:
            return int(rest[1:]),int(coefficient)

def new_terms(poly : str) -> list[tuple[int,int]]:
    return sorted([reformat_terms(t,get_var(s)) for t in terms(s)[1:]])

def make_term(nt : tuple[int,int],var: str) -> str:
    if nt[0] == 0:
        return str(nt[1])
    elif nt[0] == 1:
        return f'{nt[1]}{var}'
    else:
        return f'{nt[1]}{var}^{nt[0]}'

def mathify(s: str) -> str:
    return '+'.join([make_term(t,get_var(s)) for t in new_terms(s)]).replace('+-','-')

print(s)
print(get_var(s))
print(terms(s))
print(new_terms(s))
print(mathify(s))

#Approach 2 
def mathify(s: str) -> str:
    combo = zip(reversed(signs(s)),reversed(terms(s)))
    return ''.join([''.join(_) for _ in combo])

def terms(poly: str) -> list[str]:
    return poly.replace('+','  ').replace('-','  ').split()

def signs(poly: str) -> str:
    #if '0' <= poly.strip()[0] <= '9': (This statement doesn't hold if the coefficient of x is 1)
    if poly.strip()[0] not in '+' + '-':
        poly = '+' + poly

    
    return ''.join([ch for ch in poly if ch in '+' + '-'])

print(mathify(s1))