#Creating the layout of a  crossword
def create_crossword(rows: int, cols: int) -> tuple[tuple]:
    crossword = []
    for i in range(1,rows+1):
        for j in range(1,cols+1):
            crossword.append((i,j))
    return tuple(crossword)

def format_file(line):
    return tuple(map(int, line.strip().split(',')))


def black_boxes() -> list[tuple]:
    f=open('Crossword.txt','r')
    s=f.readlines()
    return [format_file(line) for line in s]
#print(create_crossword(15,15))
