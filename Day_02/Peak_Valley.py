#1.String question- A- string in strict ascending order, D-strict descending order, P-first part ascending and second descending, V-first part is descending and second is ascending, X-string is not any type

import sys 

def strict_ascending(word: str) -> bool:
    #return sorted(set(word)) == list(word)
    return word == ''.join(sorted(set(word)))

def strict_descending(word: str) -> bool:
    #return sorted(set(word), reverse = True) == list(word)
    return word[::-1] == ''.join(sorted(set(word)))

def is_peak(word: str) -> bool:
    sep = max(word)
    first, _, second = word.partition(sep)
    #first, second = word.split(max(word), maxsplit=1)   gives a logic error because 123321 returns true but it returns false
    return strict_ascending(first) and strict_descending(sep + second)

def is_valley(word: str) ->bool:
    first, second = word.split(max(word), maxsplit=1)
    return strict_descending(first) and strict_ascending(second)

def classify(word: str) -> str:
    if strict_ascending(word) == True:
        return 'A'
    elif strict_descending(word) == True:
        return 'D'
    elif is_peak(word) == True:
        return 'P'
    elif is_valley(word) == True:
        return 'V'
    else:
        return 'X'
for arg in sys.argv[1:]:
    print(f'{arg} is {classify(arg)}')

#Method 2 

def strictly_ascending(strings):
    return all( a < b < c for a,b,c in zip(strings,strings[1:],strings[2:]))

def strictly_descending(strings):
    return all( a > b > c for a,b,c in zip(strings, strings[1:], strings[2:]))

def no_of_peaks(strings):
    return sum( a < b > c for a,b,c in zip(strings, strings[1:], strings[2:]))

def no_of_valleys(strings):
    return sum ( a > b < c for a,b,c in zip(strings, strings[1:], strings[2:]) )

def classify(strings):
    if strictly_ascending(strings):
        return 'A'
    elif strictly_descending(strings):
        return 'D'
    elif no_of_peaks(strings):
        return 'P'
    elif no_of_valleys(strings):
        return 'V'
    else:
        return 'X'

#Approach 3 

import sys
def make_pairs(s:str) -> list[tuple[str,str]]:
    return zip([s],s[1:])
    
def encode(s: str) -> str:
    def encode_pair(pair:tuple[str,str]) -> str:
        if pair[0] < pair[1]:
            return 'A'
        elif pair[0] > pair[1]:
            return 'D'
        else:
            return 'E'
    return ''.join([encode_pair(_) for _ in make_pairs(s)])

def squeez(s: str) -> str:
    if len(s) == str:
        return s
    elif s[0] == s[1]:
        return squeez(s[1:])
    else:
        return s[0] + squeez(s[1:])
    

def classify(s : str) -> str:
    rep = squeez(encode(s))
    reps = {'A' : 'A', 'D' : 'D','AD': 'P', 'DA' : 'V'}
    if rep not in reps:
        return 'X'
    else:
        return reps[rep]

COMMA = ','  
for line in open("test_adpvx.txt"):
    data, answer = line.strip().split(COMMA)
    result = classify(data)
    if result == answer.strip():
        print("Works for {data}. Answer is {answer}")
    else:
        print("Fails for {data}. Got {result} instead of {answer}")
