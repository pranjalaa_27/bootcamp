# Print a pyramid using python imagining a screen of 40 characters
#Parameters= no of lines 

def pyramid_pattern(rows: int, screenwidth: int) -> str:
    list = []
    for i in range(1, rows):
        for j in range(i+1,0,-1): 
            print(' '*j, end = '')
        print('*'*i)

pyramid_pattern(7)

SCREEN_WIDTH = 40
SPACE = ' '
ASTERICK = '*'

def build_line(line: int) -> str:
    #return("* " * line)
    return SPACE * (SCREEN_WIDTH//2 - line) + ASTERICK * ( 2 * line + 1)

def build_pyramid(lines: int) -> str:
    
    pyramid = ''
    for line in range(lines):
        pyramid += build_line(line) + '\n'
    return pyramid 
    
    return '\n'.join([(build_line(line)) for line in range(lines)])
print(build_pyramid(11))

#3. Code to print a diamond
SCREEN_WIDTH = 40
SPACE = ' '
ASTERICK = '*'

def build_line(line: int) -> str:
    #return("* " * line)
    return SPACE * (SCREEN_WIDTH//2 - line) + ASTERICK * ( 2 * line + 1)

def build_pyramidtop(lines: int) -> str:
    return '\n'.join([(build_line(line)) for line in range(lines)])

def build_pyramidbottom(lines: int) -> str:
    return '\n'.join([(build_line(line)) for line in range(lines,-1,-1)])

print(build_pyramidtop(5))
print(build_pyramidbottom(5))


#Method 2
#Code skeleton for pattern questions
import sys
LINEWIDTH = 40
LF= '\n'
STAR = '*'
SPACE = ' '
SHARP, USCORE, BS = "#", "_", "\b"
#This method creates a skeleton of sorts in such a way that no matter what the given pattern is, if the range or idea is similar then the same skeleton can be used without manipulating
START, REPEAT, LAST = STAR, SPACE + STAR , '' 
#START, REPEAT, LAST = SHARP, USCORE + USCORE, BS + SHARP
def pattern(size: int) -> int:
    return [line(i) for i in range(size)]

def line(linenum: int) -> str:
    return start(linenum) + repeat(linenum) + end(linenum) 

def start(n: int) -> str:
    return START

def repeat(n: int) -> str:
    return REPEAT * n

def end(n: int) -> str:
    return LAST

def build_pyramid(size: int) -> str:
    return LF.join([line.center(LINEWIDTH) for line in pattern(size)])

def make_right(size: int) -> str:
    return LF.join([line.rjust(LINEWIDTH) for line in pattern(size)])

def make_diamond(size: int) -> str:
    base = pattern(size)
    diamond = base + base[::-1][1:]
    return LF.join([line.center(LINEWIDTH) for line in diamond])

def make_arrow(size: int) -> str:
    base = pattern(size)
    arrow = base + base[::-1][1:]
    return LF.join([line.ljust(LINEWIDTH) for line in arrow])
    #return LF.join([line.rjust(LINEWIDTH) for line in arrow])  #gives us an arrow that is justified to the right

def make_hourglass(size: int) -> str:
    base = pattern(size)
    hourglass = base[::-1] + base[1:]
    return LF.join([line.center(LINEWIDTH) for line in hourglass])
    
for arg in sys.argv[1:]:
    size = int(arg)
    #print(build_pyramid(size))
    #print(make_right(size))
    print(make_arrow(size),end='\n')
    print(make_diamond(size))
    #print(make_hourglass(size))
    