#Approach 1
def sieve(num: int):
    num_list = list(range(2, num))
    p = 2
    while p < num:
        for i in range( p * p, num, p):
            if i in num_list:
                num_list.remove(i)
        p += 1
    return num_list
#Approach 2

def sieve(limit):
    numbers = list(range(2, limit))

    for num in numbers:
        if num != None:
            n = num
            while num * n < limit:
                numbers[num * n - 2] = None
                n += 1

    return [num for num in numbers if num]

print(sieve(1000))