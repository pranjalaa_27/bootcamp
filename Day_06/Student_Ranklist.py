#Question - Provide a rank list for students based on their mark
#1. As a list and then as a dictionary

from itertools import combinations as nCr
SPACE, COMMENT = ' ', '#'
def parse(line: str) -> tuple[int, str]:
    line = line.strip()
    mark_start = line.rfind(SPACE)
    return int(line[mark_start:]), line[:mark_start].strip()


def load_data(filename: str) -> list[tuple[int, str]]:
    return [parse(line) for line in open(filename) if line[0] != COMMENT]


def make_ranklist(filename: str) -> list[str]:
    data = load_data(filename)
    marks = [mark for (mark, _) in data]

    def lookup(m: int) -> int:
        return 1 + len([mark for mark in marks if mark > m])
    return sorted([f'{lookup(mark):4} {name:40} {mark:4}'
                  for mark, name in data])


for line in make_ranklist("students.txt"):
    print(line)

#As a dictionary
from collections import defaultdict as ddict 
SPACE, COMMENT = ' ', '#'

def load_data(filename: str) -> dict[int, list[str]]:
    data = ddict(list)
    with open(filename) as f:
        for line in f:
            if line[0] != COMMENT:
                mark_start = line.rfind(SPACE)
                data[int(line[mark_start:])].append(line[:mark_start])
    return data


def make_ranklist(data: dict[int,list[str]]) -> list[str]:
    ranklist = []
    rank = 1
    for mark in sorted(data.keys(), reverse = True):
        for name in data[mark]:
            ranklist.append(f'{rank:4} {name:40} {mark:4}')
        rank+=len(data[mark])
    return ranklist

def write_data(filename: str):
    ranklist = make_ranklist(load_data('students.txt'))
    with open(filename,'w') as f:
        for rank in ranklist:
            f.write(rank)
            f.write('\n')
        #write to the file using a print function for better formatting
        #f.write(str(ranklist))
    return 'Ranklist Complete'

    
print(write_data('students_ranklist.txt'))


#print(make_ranklist(load_data('students.txt')))


#Question- Create a ranklist comparing 5 subjects marks, with 40 as a passmark, if a student hasn't passed, he is ranked below based on total marks
SPACE, COMMENT = ' ', '#'

SPACE, COMMENT = ' ', '#'


def parse(line: str) -> tuple[int, int, str, list[int]]:
    fields = line.strip().split()
    marks = [int(f) for f in fields if f.isdigit()]
    fails = len([mark for mark in marks if mark < 40])
    name = SPACE.join(f for f in fields if not f.isdigit())
    return fails, sum(marks), name, marks


def load_data(filename: str) -> list[tuple[int, str]]:
    return [parse(line) for line in open(filename) if line[0] != COMMENT]


def make_ranklist(filename: str) -> list[str]:
    ranklist = []
    data = sorted(load_data(filename), key=lambda rec: (rec[0], -rec[1]))
    rank = 0
    prev = 0
    for position, rec in enumerate(data, start=1):
        if rec[1] != prev:
            rank = position
            prev = rec[1]
        ranklist.append(f'{rank:4}{rec[1]:7} {rec[2]:40}{rec[3]}')
    return ranklist


for line in make_ranklist("studentmarks.txt"):
    print(line)

