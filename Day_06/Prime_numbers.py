#WAP that generates prime numbers below 1000

import math
def generate_prime(num: int) -> list[int]:
    return [i for i in range(2, num) if check_prime(i)]

def check_prime(num: int) -> bool:
    is_prime = True
    for factor in range(2,int(math.sqrt(num))+1):
        if num % factor == 0:
            is_prime = False
    return is_prime
print(generate_prime(1000))

#APPROACH 2 

def generate_prime(limit: int) -> list[int]:
    
    def is_prime(n: int) -> bool:
        factor = 7
        while factor * factor <= n:
            if n % factor == 0:
                return False
            factor += 2
        return True
        
    primes = [2, 3, 5 , 7 , 11 ,13 ,17, 19, 23 ,29]
    '''
    def is_prime(n : int) -> bool:
        pos = 3
        factor = primes[pos]
        while factor * factor <= n:
            
            if n % factor == 0:
                return False
            pos += 1
            factor = primes[pos]
        return True
    for n in range(30, limit, 30):
        for step in [1, 7, 11, 13, 17, 19, 23, 29]:
            if is_prime(n + step):
                primes.append(n + step)
    return primes 
    '''
    n1, n2 = 7, 11
    while n2 < limit:
        if is_prime(n1):
            primes.append(n1)
        if is_prime(n2):
            primes.append(n2)
        n1, n2 = n1 + 6, n2 + 6
    return primes
    for n in range(6, limit, 6):
        for step in [1, 5]:
            if is_prime(n + step):
                primes.append(n + step)
    return primes

print(generate_prime(1000))