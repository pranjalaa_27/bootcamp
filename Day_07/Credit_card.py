
def sum_of_digits(num: int) -> int:
    '''
    d = n % 9
    return 9 if d == 0 else d'''
    return 1 + (num - 1) % 9
print(sum_of_digits(8885))


def gcd_of_nums(num1: int, num2: int) -> int:
   if num1 == num2:
      return num1
   elif num1 > num2:
      return gcd_of_nums(num1 - num2, num2)
   else:
      return gcd_of_nums(num1, num2 - num1)
   
print(gcd_of_nums(252, 105))
'''

'''
def obtain_num(num: int) -> int:
    new_num = ''
    for index, i in enumerate(reversed(str(num))):
        if index % 2 == 0:
            new_num += str(int(i) * 2)
        else:
            new_num += i     #1789372997    - 4
    return new_num
    '''
'''
def obtain_num(num: int) -> int:
    n = [*str(num)]
    new_num = ''
    for i in range(len(n)-1,-1,-2):
        n[i] = str(2 * int(str(n[i])))
    for i in n:
        if len(i) != 1:
            new_num += str(sum(i))
        else:
            new_num += i
    return int(new_num)

def sum_of_digits(num: int) -> int:
    return 1 + (num - 1) % 9


def check_digit(num: int):
    n = sum_of_digits(obtain_num(num))
    return 10 - (n % 10)

print(check_digit(1789372997))

#Ideal Output
def digit_sum(num: int) -> int:
    if num < 0:
        return -9
    else:
        return 1 + (num - 1) % 9

def new_digits(n: int) -> list[int]:
    def new_digit(i: int, d: str) -> int:
        if i % 2 == 0:
            return digit_sum(int(d) + int(d))
        else:
            return int(d)
    return [new_digit(*e) for e in enumerate(reversed(str(n)))][::-1]

def check_digit(n: int) -> str:
    return 10 - (sum(new_digits(n)) % 10)

print(new_digits(1789372997))
print(check_digit(1789372997))