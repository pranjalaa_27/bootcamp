def doors_closed(limit: int) -> list[int]:
    door_case = []
    n = 1 
    while n * n < limit: 
            door_case.append(n * n)
            n += 1
    return door_case

print(doors_closed(100))