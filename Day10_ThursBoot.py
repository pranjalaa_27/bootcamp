
'''                        Session on Cryptography by Vachinathan Pichai

1. Take blocks of 4 characters.  Regard each character as ASCII number, 0 to 255. Now blocks of 4 characters as a message unit.  Read it as a number of 4-digits in base 256.
GOOD| MOR|NING
G - 71 O - 79 O - 79 D - 68 | _  - 32 M - 77 O - 79 R - 82 | N - 78 I - 73 N - 78 G - 71
Preprocessor : 71797968 32777982 78737871

8 * 4 = 32
32 % 31 = 1
(8 * 4) % 33 = 1
'''
#Generate a random 5 letter string 
'''
import random
def randomstring(num: int) -> str:
    l = [random.randint(65,90) for i in range(num)]
    random_string = ''
    for i in l:
        random_string = random_string + chr(i)
    return random_string

print(randomstring(5))

#Generate based on the frequencies of alphabets 
import random
def get_freq(filename) -> list[list]:
    with open(filename, 'r') as freq:
        char_freq = []
        char = []
        for line in freq:
            data = line.strip('\n').strip('%').split(',')
            char.append(data[0])
            char_freq.append(data[1]) 
        return char[1:], char_freq[1:]

def get_string(filename: str, num: int) -> str:
    char = get_freq(filename)
    for i in range(len(char[0])): 
        char[1][i] = round(float(char[1][i])*0.01 , 4)
    string_req =''.join(random.choices(char[0], weights = char[1], k = num)) 
    return string_req

def current_freq(filename: str) -> list[list]:
    print(1)

print(get_string('letter_frequencies.csv', 22))
'''

def get_data(filename: str) -> str:
    with open(filename, 'r') as f:
        char = f.read()
        return char


def decode_data(filename: str) -> dict[str]:
    data = get_data(filename)
    freq = {}
    total = 0.0
    for char in data:
        if char in freq.keys():
            freq[char] += 1
        else:
            freq[char] = 1
    for key in freq:
        total += freq[key]
        freq[key] = round((freq[key]/total)*100, 3)
    return freq 

def display_data(filename: str):
    print('Character \t\tFrequency_1')
    freq = decode_data(filename)
    for i in freq.items():
        print(i[0],'\t\t',i[1],'%')






print(display_data('mono_ciph017.txt'))