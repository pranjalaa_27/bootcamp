import bishop
import knight
import rook
import Queen


def check_attack(piece: str, pos: str) -> list[str]:
    if piece.lower() == 'queen':
        return Queen.queen_moves(pos)
    elif piece.lower() == 'knight':
        return knight.knight_moves(pos)
    elif piece.lower() == 'bishop':
        return bishop.bishop_moves(pos)
    else:
        return rook.rook_moves(pos)

def is_attacking(piece1: str, pos1: str, piece2: str, pos2: str) -> list[str]:
    piece1_attack = check_attack(piece1, pos1)
    piece2_attack = check_attack(piece2, pos2)
    if pos2 in piece1_attack:
        return str(f'({piece1.capitalize()} {pos1} can attack {piece2.capitalize()} {pos2})')
    elif pos1 in piece2_attack:
        return str(f'({piece2.capitalize()} {pos2} can attack {piece1.capitalize()} {pos1})')
    else:
        return "No attacks occur"

print(is_attacking('queen', 'e3', 'rook', 'e5'))