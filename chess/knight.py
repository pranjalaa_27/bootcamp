files = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
ranks = [1, 2, 3, 4, 5, 6, 7, 8]
def is_valid(row: str, column: int) -> bool:
    if row in files:
        if column in ranks:
            return True
        else:
            return False

def knight_moves(pos: str) -> list:
    possible_cordinates = []
    possible_movements = [(2, 1), (2, -1), (-2, 1), (-2, -1), (1,2), (-1,2), (1,-2), (-1, -2)]
    row = ord(pos[0])-ord('a') + 1
    column = int(pos[1]) 
    for move_point in possible_movements:
        row_point = chr(row + move_point[1] - 1 + ord('a'))
        column_point =  column + move_point[0]        
        if is_valid(row_point, column_point):
            new_cordinates = str(row_point) + str(column_point)
            possible_cordinates.append(new_cordinates)
    
    return possible_cordinates

#print(knight("a8"))