def alpha_to_num(point: str) -> list[int]:
    ranks = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
    files = [1, 2, 3, 4, 5, 6, 7, 8]
    points = list(point)
    for i in range(len(ranks)):
        if points[0] == ranks[i]:
            points[0] = files[i]
    return points[0], int(points[1])

def positions_of_bishops(point: str) -> list[int]:
    positions = []
    x, y = alpha_to_num(point)

    top_left_diagonal = min(x, y)
    top_right_diagonal = min(x, 8 - y)

    bottom_left_diagonal = min(8 - x, y)
    bottom_right_diagonal = min(8 - x, 8 - y)
    
    positions.extend([[x - i, y - i] for i in range(1,top_left_diagonal + 1 )]) 
    positions.extend([[x + i, y - i] for i in range(1, top_right_diagonal + 1)])
    positions.extend([[x - i, y + i] for i in range(1, bottom_left_diagonal + 1)])
    positions.extend([[x + i, y + i] for i in range(1, bottom_right_diagonal+1)]) 
    
    return sorted(positions)

def bishop_moves(position: str) -> list[str] :
    bishops = []
    ranks = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
    files = [1, 2, 3, 4, 5, 6, 7, 8]
    
    for position in positions_of_bishops(position):
        for i in range(8):
            if files[i] == position[0]:
                bishops.append(ranks[i]+str(position[1]))
    

    return bishops

print(bishop_moves('d3'))
